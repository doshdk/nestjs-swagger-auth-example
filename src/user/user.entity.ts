import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    unique: true,
  })
  username: string;

  @Column()
  password: string;

  @Column()
  firstName: string;

  @Column()
  surName: string;

  @Column({
    unique: true,
  })
  email: string;

  @Column({
    default: false,
  })
  active: boolean;
}
