import { ConflictException, Injectable, InternalServerErrorException } from '@nestjs/common';
import { User } from './user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from '../auth/dto/create-user.dto';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {

  constructor(@InjectRepository(User)private readonly userRepository: Repository<User>) {

  }

  async createUser(createUserDto: CreateUserDto) {
    try {
      const insertion = await this.userRepository.insert(createUserDto);
      return await this.userRepository.find(insertion.raw.insertId);
    } catch (e) {
      if (e.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('A user with provided email/username already exists');
      }
      throw new InternalServerErrorException();
    }
  }

  async findOne(username: string): Promise<User> {
    return this.userRepository.findOne({
      username,
    });
  }
}
