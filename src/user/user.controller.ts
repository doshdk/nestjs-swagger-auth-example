import { Controller, Get, UseGuards, Request } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { UserService } from './user.service';
import { ApiTags, ApiHeader } from '@nestjs/swagger';

@ApiTags('User')
@Controller('user')
export class UserController {

  constructor(private readonly userService: UserService) {}

  @ApiHeader({
    name: 'Authorization',
    description: '"Bearer " + Token',
  })
  @UseGuards(AuthGuard('jwt'))
  @Get('profile')
  async getProfile(@Request() req) {
    const {password, ...result} = await this.userService.findOne(req.user.username)
    return result;
  }

}
