import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import * as bcrypt from 'bcrypt';
import { UserService } from '../user/user.service';
const saltRounds = process.env.SALT_ROUNDS || 10;
const salt = bcrypt.genSaltSync(saltRounds);

@Injectable()
export class AuthService {

  constructor(private readonly jwtService: JwtService, private userService: UserService) {}

  generateHash(plainTextPassword: string) {
    return bcrypt.hashSync(plainTextPassword, salt);
  }

  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.userService.findOne(username);
    if (user && bcrypt.compareSync(pass, user.password)) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  login(user: any) {
    return {
      access_token: this.jwtService.sign({username: user.username, sub: user.id}),
    };
  }
}
