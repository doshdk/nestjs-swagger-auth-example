import { Body, Controller, Post, UseGuards, Request } from '@nestjs/common';

import { CreateUserDto } from './dto/create-user.dto';
import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { AuthGuard } from '@nestjs/passport';
import { LoginDto } from './dto/login.dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {

  constructor(private authService: AuthService, private userService: UserService) {}

  @Post('register')
  register(@Body() body: CreateUserDto) {
    const password = this.authService.generateHash(body.password);

    this.userService.createUser({ ...body, password });
  }

  @UseGuards(AuthGuard('local'))
  @Post('login')
  login(@Request() req, @Body() loginDto: LoginDto) {
    return this.authService.login(req.user);
  }
}
