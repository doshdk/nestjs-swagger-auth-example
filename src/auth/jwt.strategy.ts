import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET || 'mysupersecretpassword',
    });
  }

  async validate(payload: any) {

    /*
    @TODO: Add redis blacklisting
     */
    if(false) {
      throw new UnauthorizedException();
    }

    return { id: payload.sub, username: payload.username };
  }
}
